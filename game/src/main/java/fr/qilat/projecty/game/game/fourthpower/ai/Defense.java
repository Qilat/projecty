package fr.qilat.projecty.game.game.fourthpower.ai;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Defense {

    @Getter
    private List<Scheme> defenses;

    public Defense(List<Scheme> defenses) {
        this.defenses = defenses;
    }

    public List<Scheme> aboutToLoose() {
        List<Scheme> finalSchemes = new ArrayList<>();
        for (Scheme defense : defenses) {
            if (defense.getNbOfElementsFilled() == defense.getMaxNbOfElement() - 1) {
                finalSchemes.add(defense);
            }
        }
        return finalSchemes;
    }

    public List<Scheme> getMostBoringDefenses() {
        List<Scheme> schemes = new ArrayList<>();
        int maxAmountFound = 0;
        for (Scheme defense : defenses) {
            int nbOFElementsFilled = defense.getNbOfElementsFilled();
            if (nbOFElementsFilled > maxAmountFound) {
                schemes = new ArrayList<>();
                schemes.add(defense);
            } else if (nbOFElementsFilled == maxAmountFound) {
                schemes.add(defense);
            }
        }
        return schemes;
    }
}
