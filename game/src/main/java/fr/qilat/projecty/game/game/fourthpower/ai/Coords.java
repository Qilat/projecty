package fr.qilat.projecty.game.game.fourthpower.ai;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Coords {

    @Getter
    @Setter
    private int columnId;

    @Getter
    @Setter
    private int rowId;

    public Coords(int columnId, int rowId) {
        this.columnId = columnId;
        this.rowId = rowId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) return false;
        if (obj == this) return true;

        Coords coords = (Coords) obj;
        return this.columnId == coords.getColumnId()
                && this.rowId == coords.getRowId();
    }
}
