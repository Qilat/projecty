package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.graph.Material;
import fr.qilat.projecty.game.engine.graph.Mesh;
import fr.qilat.projecty.game.engine.graph.OBJLoader;
import fr.qilat.projecty.game.engine.items.GameItem;
import lombok.Getter;
import org.joml.Vector4f;

/**
 * Created by Theophile on 2018-12-14 for projecty.
 */
public class Grid extends GameItem {

    private static final String OBJ_FILE = "/models/grid.obj";

    @Getter
    private GridContent content;

    public Grid() throws Exception {
        super();
        Mesh gridMesh = OBJLoader.loadMesh(OBJ_FILE);
        Material material = new Material(new Vector4f(0.0f, 0.0f, 1.0f, 1.0f), 0.0f);
        gridMesh.setMaterial(material);
        this.setMesh(gridMesh);
        this.setPosition(0, 0, 0);

        this.content = new GridContent(7, 6, 4);
    }

    public void updateChipsPositions(float interval) {
        Chip[][] chips = this.getContent().getChips();
        for (int i = 0; i < chips.length; i++) {
            for (int j = 0; j < chips[0].length; j++) {
                Chip chip = chips[i][j];
                if (chip != null && chip.isMoving()) {
                    chip.setMovingPosition(interval);
                }
            }
        }

    }

}
