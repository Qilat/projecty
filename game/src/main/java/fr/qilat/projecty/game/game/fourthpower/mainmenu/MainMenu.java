package fr.qilat.projecty.game.game.fourthpower.mainmenu;

import fr.qilat.projecty.game.engine.IGameLogic;
import fr.qilat.projecty.game.engine.MouseInput;
import fr.qilat.projecty.game.engine.Scene;
import fr.qilat.projecty.game.engine.Window;
import fr.qilat.projecty.game.engine.graph.Camera;
import fr.qilat.projecty.game.engine.graph.Renderer;
import org.joml.Vector3f;


/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class MainMenu implements IGameLogic {

    private final Renderer renderer;
    private final Camera camera;
    private Scene scene;
    private MainMenuHud hud;

    public MainMenu() {
        renderer = new Renderer();
        camera = new Camera(new Vector3f(0f, 0f, 0f), new Vector3f(0f, 0f, 0));
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        scene = new Scene();
        hud = new MainMenuHud();
    }

    @Override
    public void input(Window window, MouseInput mouseInput) throws Exception {
        if (mouseInput.isLeftButtonPressed()) {
            hud.handleClick(mouseInput.getCurrentPos().x, mouseInput.getCurrentPos().y);
        }
    }

    @Override
    public void update(float interval, MouseInput mouseInput) {
    }

    @Override
    public void render(Window window) {
        hud.updateSize(window);
        renderer.render(window, camera, scene, hud);
    }

    @Override
    public void cleanup() {
        this.renderer.cleanup();
        if (this.scene != null)
            this.scene.cleanup();
        if (this.hud != null)
            this.hud.cleanup();
    }
}
