package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.graph.Material;
import fr.qilat.projecty.game.engine.graph.Mesh;
import fr.qilat.projecty.game.engine.graph.OBJLoader;
import fr.qilat.projecty.game.engine.items.GameItem;
import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4f;

import java.util.Objects;

/**
 * Created by Theophile on 2018-12-14 for projecty.
 */
public class Chip extends GameItem {

    private static final String MODEL_PATH = "/models/chip.obj";

    private static final float[] COORDS = new float[]{-6.3f, 4.75f, 0f};
    private static final float deltaX = 5.5f;
    private static final float deltaY = 5.5f;
    private static final float startY = 37f;
    private static final float speed = 30f;

    @Getter
    private Vector4f color;
    @Setter
    @Getter
    private boolean moving;
    @Setter
    @Getter
    private int column;
    @Setter
    @Getter
    private int row;
    @Getter
    private Player player;

    public Chip(Player player, int column, int row) {
        try {
            this.player = player;
            this.color = player.getColor();
            this.column = column;
            this.row = row;
            Material material = new Material(this.color, 1.0f);
            Mesh mesh = OBJLoader.loadMesh(MODEL_PATH);
            mesh.setMaterial(material);
            this.setMesh(mesh);
            this.setScale(0.55f);
            this.setPosition(COORDS[0] + (deltaX * column), COORDS[1] + (startY), COORDS[2]);
            this.moving = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chip chip = (Chip) o;
        return Objects.equals(color, chip.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }

    public void setMovingPosition(float interval) {
        if (this.getPosition().y > (COORDS[1] + (this.row * deltaY))) {
            this.setPosition(
                    this.getPosition().x,
                    this.getPosition().y - speed * interval,
                    this.getPosition().z
            );
        } else {
            if (this.getPosition().y <= (COORDS[1] + (this.row * deltaY))) {
                this.setPosition(this.getPosition().x, (COORDS[1] + (this.row * deltaY)), this.getPosition().z);
                this.setMoving(false);
            }
        }
    }
}
