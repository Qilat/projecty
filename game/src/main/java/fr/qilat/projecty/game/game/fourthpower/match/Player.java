package fr.qilat.projecty.game.game.fourthpower.match;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4f;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Player {

    @Getter
    @Setter
    private Vector4f color;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private boolean isAI = false;

    public Player(String name, Vector4f color) {
        this.name = name;
        this.color = color;
    }


}
