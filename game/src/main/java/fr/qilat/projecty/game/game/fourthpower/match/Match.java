package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.Scene;
import fr.qilat.projecty.game.engine.input.InputListener;
import fr.qilat.projecty.game.engine.input.InputManager;
import fr.qilat.projecty.game.game.fourthpower.ai.AI;
import lombok.Getter;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Theophile on 2018-12-14 for projecty.
 */
public class Match {

    @Getter
    private Player player1;
    @Getter
    private Player player2;

    @Getter
    private Player playingPlayer;

    @Getter
    private Scene scene;
    @Getter
    private Selector selector;
    @Getter
    private Grid grid;

    public Match(Player player1, Player player2, Scene scene) throws Exception {
        this.player1 = player1;
        this.player2 = player2;
        this.scene = scene;

        this.playingPlayer = player1;

        this.grid = new Grid();

        this.selector = new Selector(player1.getColor(), 7);
        InputManager.get().registerListener(this);
    }

    public boolean addChip() {
        return this.grid.getContent().addChipToColumn(scene, selector.getSelectedPosition(), playingPlayer);
    }

    private boolean addChip(int selectedPosition) {
        return this.grid.getContent().addChipToColumn(scene, selectedPosition, playingPlayer);
    }

    public void switchPlayingPlayer() throws Exception {
        if (playingPlayer.equals(player1)) {
            this.setPlayingPlayer(player2);
        } else {
            this.setPlayingPlayer(player1);
        }

        if (playingPlayer.isAI()) {
            AI ai = (AI) playingPlayer;
            int columnId = ai.analyzeAndAct(ai, this.grid.getContent(), this.grid.getContent().getAmountToWon());
            addChip(columnId);
            this.switchPlayingPlayer();
        }

    }

    public void setPlayingPlayer(Player player) throws Exception {
        if (player.equals(player1) || player.equals(player2)) {
            this.playingPlayer = player;
            this.selector.setColor(player.getColor());
        } else {
            throw new Exception("This player is not playing");
        }
    }

    @InputListener
    public void onInput(int keyCode) throws Exception {
        switch (keyCode) {
            case GLFW_KEY_RIGHT:
                this.getSelector().incrementPosition();
                break;
            case GLFW_KEY_LEFT:
                this.getSelector().decrementPosition();
                break;
            case GLFW_KEY_ENTER:
                if (this.addChip()) {
                    this.switchPlayingPlayer();
                }
                break;
        }
    }

}
