package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.IHud;
import fr.qilat.projecty.game.engine.Window;
import fr.qilat.projecty.game.engine.graph.FontTexture;
import fr.qilat.projecty.game.engine.items.GameItem;
import fr.qilat.projecty.game.engine.items.TextItem;
import fr.qilat.projecty.game.game.fourthpower.mainmenu.ImageItem;
import org.joml.Vector4f;

import java.awt.*;

public class Hud implements IHud {

    private static final Font FONT = new Font("Arial", Font.PLAIN, 20);

    private static final String CHARSET = "ISO-8859-1";

    private final GameItem[] gameItems;

    private final TextItem statusTextItem;
    private final ImageItem imageItem;

    public Hud(String statusText) throws Exception {
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        this.statusTextItem = new TextItem(statusText, fontTexture);
        this.statusTextItem.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 1, 1, 1));

        this.imageItem = new ImageItem();

        // Create list that holds the items that compose the HUD
        gameItems = new GameItem[]{statusTextItem};
    }

    public void setStatusText(String statusText) {
        this.statusTextItem.setText(statusText);
    }

    @Override
    public GameItem[] getGameItems() {
        return gameItems;
    }

    public void updateSize(Window window) {
        this.statusTextItem.setPosition(10f, window.getHeight() - 50f, 0);
    }
}
