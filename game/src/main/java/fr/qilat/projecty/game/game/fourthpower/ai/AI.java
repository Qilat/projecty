package fr.qilat.projecty.game.game.fourthpower.ai;

import fr.qilat.projecty.game.game.fourthpower.match.Chip;
import fr.qilat.projecty.game.game.fourthpower.match.GridContent;
import fr.qilat.projecty.game.game.fourthpower.match.Player;
import lombok.Getter;
import org.joml.Vector4f;

import java.util.*;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class AI extends Player {

    private Scheme lastAttackScheme = null;

    @Getter
    private boolean hasPlayed = false;

    public AI(String name, Vector4f color) {
        super(name, color);
    }

    private static Scheme scanRightHorizontal(Chip[][] chips, int amountToWon, int column, int row) {
        if (column <= (chips.length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column + i][row];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column + i, row), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column + i, row), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanLeftHorizontal(Chip[][] chips, int amountToWon, int column, int row) {
        if (column >= (chips.length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column - i][row];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column - i, row), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column - i, row), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanUpVertical(Chip[][] chips, int amountToWon, int column, int row) {
        if (row <= (chips[0].length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column][row + i];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column, row + i), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column, row + i), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanAscendantRightDiag(Chip[][] chips, int amountToWon, int column, int row) {
        if (row <= (chips[0].length - amountToWon)
                && column <= (chips.length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column + i][row + i];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column + i, row + i), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column + i, row + i), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanAscendantLeftDiag(Chip[][] chips, int amountToWon, int column, int row) {
        if (row <= (chips[0].length - amountToWon)
                && column >= (chips.length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column - i][row + i];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column - i, row + i), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column - i, row + i), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanDescendantRightDiag(Chip[][] chips, int amountToWon, int column, int row) {
        if (row >= (amountToWon - 1)
                && column <= (chips.length - amountToWon)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column + i][row - i];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column + i, row - i), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column + i, row - i), false);
                }
            }
            return scheme;
        }
        return null;
    }

    private static Scheme scanDescendantLeftDiag(Chip[][] chips, int amountToWon, int column, int row) {
        if (row >= (amountToWon - 1)
                && column >= (amountToWon - 1)) {
            Chip chip = chips[column][row];
            Scheme scheme = new Scheme(amountToWon);
            for (int i = 0; i < amountToWon; i++) {
                Chip checkedChip = chips[column - i][row - i];
                if (checkedChip != null) {
                    if (checkedChip.getPlayer().equals(chip.getPlayer())) {
                        scheme.addState(new Coords(column - i, row - i), true);
                    } else {
                        return null;
                    }
                } else {
                    scheme.addState(new Coords(column - i, row - i), false);
                }
            }
            return scheme;
        }
        return null;
    }

    public int analyzeAndAct(Player ai, GridContent content, int amountToWon) {
        this.hasPlayed = true;
        Defense defense = getPossibleDefenses(ai, content, amountToWon);
        Attack attack = getPossibleDirectAttacks(ai, content, amountToWon);
        List<Scheme> aboutToLoose = defense.aboutToLoose();
        List<Scheme> aboutToWin = attack.aboutToWin();
        Random random = new Random();
        if (aboutToWin.size() > 0) {
            return constructAttackMove(ai, content, aboutToWin.get(random.nextInt(aboutToWin.size())), random);
        }
        if (aboutToLoose.size() > 0) {
            return constructDefenseMove(ai, content, aboutToLoose.get(random.nextInt(aboutToLoose.size())), random);
        }
        if (defense.getDefenses().isEmpty()
                && attack.getAttacks().isEmpty()) {
            return constructRandomMove(ai, content, random);
        }
        int attackOrDefense = random.nextInt(10);
        int attackBoundary = 7;
        if (attackOrDefense > attackBoundary) {
            List<Scheme> mostPromisings = attack.getMostPromisingAttack();
            if (mostPromisings.contains(lastAttackScheme)) {
                return constructAttackMove(ai, content, lastAttackScheme, random);
            } else {
                if (!mostPromisings.isEmpty()) {
                    lastAttackScheme = mostPromisings.get(random.nextInt(mostPromisings.size()));
                } else {
                    return constructRandomMove(ai, content, random);
                }
                return constructAttackMove(ai, content, lastAttackScheme, random);
            }
        } else {
            List<Scheme> mostBorings = defense.getMostBoringDefenses();
            return constructDefenseMove(ai, content, mostBorings.get(random.nextInt(mostBorings.size())), random);
        }

    }

    private int constructAttackMove(Player ai, GridContent content, Scheme scheme, Random random) {
        List<Coords> playableCoords = scheme.getPlayableCoords();
        List<Coords> urgentCoords = getUrgentCoords(content, playableCoords);

        if (!urgentCoords.isEmpty()) {
            return urgentCoords.get(random.nextInt(urgentCoords.size())).getColumnId();
        } else {
            List<Coords> fineCoords = new ArrayList<>();
            for (Coords playableCoord : playableCoords) {
                if (Math.abs(getHigherChipRow(content.getChips()[playableCoord.getColumnId()]) - playableCoord.getRowId()) != 2) {
                    fineCoords.add(playableCoord);
                }
            }

            if (!fineCoords.isEmpty())
                return fineCoords.get(random.nextInt(fineCoords.size())).getColumnId();

            //TODO Change Scheme if non satisfaisant move found in this scheme
            return playableCoords.get(random.nextInt(playableCoords.size())).getColumnId();
        }
    }

    private int constructDefenseMove(Player ai, GridContent content, Scheme scheme, Random random) {
        List<Coords> playableCoords = scheme.getPlayableCoords();
        List<Coords> urgentCoords = getUrgentCoords(content, playableCoords);

        if (!urgentCoords.isEmpty()) {
            return urgentCoords.get(random.nextInt(urgentCoords.size())).getColumnId();
        } else {
            List<Coords> fineCoords = new ArrayList<>();
            for (Coords playableCoord : playableCoords) {
                if (Math.abs(getHigherChipRow(content.getChips()[playableCoord.getColumnId()]) - playableCoord.getRowId()) != 2) {
                    fineCoords.add(playableCoord);
                }
            }

            if (!fineCoords.isEmpty())
                return fineCoords.get(random.nextInt(fineCoords.size())).getColumnId();

            //TODO Change Scheme if non satisfaisant move found in this scheme
            return playableCoords.get(random.nextInt(playableCoords.size())).getColumnId();
        }
    }

    private int constructRandomMove(Player ai, GridContent content, Random random) {
        return random.nextInt(content.getChips().length);
    }

    private int getHigherChipRow(Chip[] column) {
        for (int i = column.length - 1; i >= 0; i--) {
            if (column[i] != null)
                return i;
        }
        return 0;
    }

    private List<Coords> getUrgentCoords(GridContent content, List<Coords> coords) {
        List<Coords> urgences = new ArrayList<>();
        for (Coords coord : coords) {
            if (isCoordUrgentToPlay(content, coord))
                urgences.add(coord);
        }
        return urgences;
    }

    private boolean isCoordUrgentToPlay(GridContent content, Coords coords) {
        if (coords.getRowId() == 0) return true;
        Chip[] column = content.getChips()[coords.getColumnId()];
        return column[coords.getRowId() - 1] != null;
    }

    private Attack getPossibleDirectAttacks(Player ai, GridContent content, int amountToWon) {
        List<Scheme> schemes = new ArrayList<>();
        Chip[][] chips = content.getChips();
        for (int column = 0; column < chips.length; column++) {
            for (int row = 0; row < chips[0].length; row++) {
                Chip chip = chips[column][row];
                if (chip != null && chip.getPlayer().equals(ai)) {
                    Scheme rightHorizontal = scanRightHorizontal(chips, amountToWon, column, row);
                    if (rightHorizontal != null)
                        schemes.add(rightHorizontal);
                    Scheme leftHorizontal = scanLeftHorizontal(chips, amountToWon, column, row);
                    if (leftHorizontal != null)
                        schemes.add(leftHorizontal);
                    Scheme upVertical = scanUpVertical(chips, amountToWon, column, row);
                    if (upVertical != null)
                        schemes.add(upVertical);
                    Scheme ascRightDiag = scanAscendantRightDiag(chips, amountToWon, column, row);
                    if (ascRightDiag != null)
                        schemes.add(ascRightDiag);
                    Scheme ascLeftDiag = scanAscendantLeftDiag(chips, amountToWon, column, row);
                    if (ascLeftDiag != null)
                        schemes.add(ascLeftDiag);
                    Scheme descRightDiag = scanDescendantRightDiag(chips, amountToWon, column, row);
                    if (descRightDiag != null)
                        schemes.add(descRightDiag);
                    Scheme descLeftDiag = scanDescendantLeftDiag(chips, amountToWon, column, row);
                    if (descLeftDiag != null)
                        schemes.add(descLeftDiag);
                }
            }
        }
        schemes.sort(Collections.reverseOrder(Comparator.comparingInt(Scheme::getNbOfElementsFilled)));
        return new Attack(schemes);
    }

    private Defense getPossibleDefenses(Player ai, GridContent content, int amountToWon) {
        List<Scheme> schemes = new ArrayList<>();
        Chip[][] chips = content.getChips();
        for (int column = 0; column < chips.length; column++) {
            for (int row = 0; row < chips[0].length; row++) {
                Chip chip = chips[column][row];
                if (chip != null && !chip.getPlayer().equals(ai)) {
                    Scheme rightHorizontal = scanRightHorizontal(chips, amountToWon, column, row);
                    if (rightHorizontal != null)
                        schemes.add(rightHorizontal);
                    Scheme leftHorizontal = scanLeftHorizontal(chips, amountToWon, column, row);
                    if (leftHorizontal != null)
                        schemes.add(leftHorizontal);
                    Scheme upVertical = scanUpVertical(chips, amountToWon, column, row);
                    if (upVertical != null)
                        schemes.add(upVertical);
                    Scheme ascRightDiag = scanAscendantRightDiag(chips, amountToWon, column, row);
                    if (ascRightDiag != null)
                        schemes.add(ascRightDiag);
                    Scheme ascLeftDiag = scanAscendantLeftDiag(chips, amountToWon, column, row);
                    if (ascLeftDiag != null)
                        schemes.add(ascLeftDiag);
                    Scheme descRightDiag = scanDescendantRightDiag(chips, amountToWon, column, row);
                    if (descRightDiag != null)
                        schemes.add(descRightDiag);
                    Scheme descLeftDiag = scanDescendantLeftDiag(chips, amountToWon, column, row);
                    if (descLeftDiag != null)
                        schemes.add(descLeftDiag);
                }
            }
        }
        schemes.sort(Collections.reverseOrder(Comparator.comparingInt(Scheme::getNbOfElementsFilled)));
        return new Defense(schemes);
    }
}
