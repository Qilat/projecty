package fr.qilat.projecty.game.engine.items;

import fr.qilat.projecty.game.engine.graph.FontTexture;
import lombok.Getter;
import org.joml.Vector3f;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class ButtonItem extends TextItem {

    @Getter
    private Vector3f size;

    public ButtonItem(Vector3f size, String text, FontTexture fontTexture) throws Exception {
        super(text, fontTexture);
        this.size = size;
    }

    public boolean isClicked(double mouseX, double mouseY) {
        System.out.println(this.getPosition().x + " -> " + mouseX + " / " + this.getPosition().y + " -> " + mouseY);
        return mouseX > this.getPosition().x
                && mouseX < (this.getPosition().x + this.getSize().x)
                && mouseY > this.getPosition().y
                && mouseY < (this.getPosition().y + this.getSize().y);
    }

    public void onClick() {
        System.out.println("Hello");
    }
}
